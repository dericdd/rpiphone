#!/usr/bin/php
<?php
set_time_limit(3600);
/**
 * -Written by Deric D. Davis deric@deric.ca.
 *
 * 1) Install required software and applications.
 * 2) Setup LAN & DHCP
 * 3) Setup WAN - with iPhone USB connectivity
 * 4) Setup IPTables
 */


// >> Config Variables:
$baseDir      = dirname(__FILE__);
$resourcesDir = "{$baseDir}/resources";
$isRoot       = posix_getuid()==0 ? true : false;

// Interfaces:
$ifaceLAN     = 'eth0'; // EG: RPI Ethernet Port
$ifaceWAN     = 'eth1'; // IE: USB iPhone connection

echo "-- RPiPhone Network Setup --\n";
echo "Current User: ".get_current_user()." (".(!empty($isRoot)?'root':'user')." access)\n";
echo "Base Dir: {$baseDir}\n";
echo "Resources Dir: {$resourcesDir}\n";
echo "\n";



// >> CHECKS <<
// Check Root Access:
if(empty($isRoot)) exit("ERROR: You need root access to run this command! Please run command with 'sudo' or as root user...\n\n");

// count steps:
$step = 1;






// >>> 1) Install Required Software: <<<

echo ">> ".$step++."}) Update and Install System Software. <<\n";

// Update Installation:
echo "-- Updating APT:\n";
system("apt-get -y update");

echo "-- System Update:\n";
system("apt-get -y upgrade");

echo "-- Dist Upgrade:\n";
system("apt-get -y dist-upgrade");

// Install CMD & INSTALL:
$installCmd  = "apt-get -y install ";
$installCmd .= " iptraf nmap traceroute "; // Basic network monitoring stuff.
$installCmd .= " hostapd udhcpd "; // DHCP stuff.
$installCmd .= " gvfs ipheth-utils ifuse libimobiledevice-utils gvfs-backends gvfs-bin gvfs-fuse "; // iPhone stuff
echo "-- Install required software:\n";
system($installCmd);





// >>> 2) Setup LAN: <<<

echo ">> ".$step++."}) Setup LAN. <<\n";

// Backup Original Interface File & Copy Example Interface File:
echo "-- Copy LAN network interface files.\n";
system("cp /etc/network/interfaces /etc/network/interfaces.orig");
system("cp {$resourcesDir}/interfaces /etc/network/");

// Copy Example DHCP file:
if(is_file("/etc/udhcpd.conf")) system("cp /etc/udhcpd.conf /etc/udhcpd.conf.orig"); 

echo "-- Configure DHCP on LAN.\n";
system("cp {$resourcesDir}/udhcpd.conf /etc/");
system("cp {$resourcesDir}/udhcpd_default /etc/default/udhcpd");

echo "-- Add uDHCP to RC.d.\n";
system("sudo update-rc.d udhcpd enable");





// >>> 3) Setup WAN (iPhone): <<<

echo ">> ".$step++."}) Setting up iPhone WAN:\n";

// Make iPhone Fuse Directory:
echo "-- Create iPhone fuse dir:\n";
if(!is_dir('/media/iPhone')) mkdir('/media/iPhone');

// Get rc.local suppliment:
$rcSup   = file_get_contents("{$resourcesDir}/rc.local-sup.txt");
if(empty($rcSup)) exit("ERROR: Failed to load RC-Sup!\n\n");

// Get Existing rc.local file content:
$rcLocal = file_get_contents("/etc/rc.local");
if(stristr($rcLocal, 'ipheth-pair')) exit("ERROR: The file rc.local already contains reference to 'ipheth-pair' command. Please remove it and start over...\n\n");

// Write new rc.local file:
echo "-- Write new rc.local file:\n";
$newRcLocal = str_replace("exit 0", "{$rcSup}\nexit 0", $rcLocal);
file_put_contents('/etc/rc.local', $newRcLocal);






// >>> 4) ipTables Setup: <<<

/*  > EG IpTables:
	sudo iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
	sudo iptables -A FORWARD -i eth1 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i eth0 -o eth1 -j ACCEPT
*/

echo ">> ".$step++."}) Setting up Routing with IPTables:\n";

echo "-- Enable IP Forwarding.\n";
system("sudo sh -c \"echo 1 > /proc/sys/net/ipv4/ip_forward\"");

echo "-- Enable IP Forwarding on Boot.";
system("echo \"net.ipv4.ip_forward=1\" >> /etc/sysctl.conf");

echo "-- Configure IPTables.\n";
system("sudo iptables -t nat -A POSTROUTING -o {$ifaceWAN} -j MASQUERADE");
system("sudo iptables -A FORWARD -i {$ifaceWAN} -o {$ifaceLAN} -m state --state RELATED,ESTABLISHED -j ACCEPT");
system("sudo iptables -A FORWARD -i {$ifaceLAN} -o  {$ifaceWAN} -j ACCEPT");

echo "-- Save IPTables Config.\n";
system("sudo sh -c \"iptables-save > /etc/iptables.ipv4.nat\"");



// Saying final goodbyes...
echo "\nInstallation complete. Shutting down and halting system...\n";
echo "... buh-bye ...\n\n";
system("halt");





